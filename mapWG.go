package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

type student struct {
	grades map[string]int
}

func main() {

	var (
		wg  sync.WaitGroup
		val atomic.Value
	)

	val.Store(student{
		grades: make(map[string]int),
	})

	wg.Add(3)

	go func() {
		defer wg.Done()
		s := val.Load().(student)
		s.grades["English"] = 90
	}()

	go func() {
		defer wg.Done()
		s := val.Load().(student)
		s.grades["Programming"] = 100
	}()

	go func() {
		defer wg.Done()
		s := val.Load().(student)
		s.grades["Math"] = 70
	}()

	wg.Wait()
	s := val.Load().(student)
	for k, v := range s.grades {
		fmt.Println(k,"---->",v)
	}
}

