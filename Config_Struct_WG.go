package main

import (
	"fmt"
	"log"
	"sync"
	"sync/atomic"
)

type Config struct {
	a []int
}

func main() {
	var (
		wg sync.WaitGroup
		v  atomic.Value
	)
	v.Store(Config{a: []int{}})
	//wg.Add(1)
	go func() {
		var i int
		i = 0
		for {
			i++
			cfg := Config{
				a: []int{i + 1, i + 2, i + 3, i + 4, i + 5},
			}
			v.Store(cfg)
		}
	}()
	//time.Sleep(time.Millisecond * 1)
	wg.Add(50)
	for i := 0; i < 50; i++ {
		go func() {
			defer wg.Done()
			cfg, ok := v.Load().(Config)
			if !ok {
				log.Fatalf("Received a Different type: %T\n", cfg)
			}
			fmt.Println("Cfg: ", cfg)
		}()
	}
	wg.Wait()
}

