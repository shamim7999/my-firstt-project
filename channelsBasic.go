package main

import (
	"fmt"
	"sync"
)

func main() {
	mych := make(chan []int, 1)
	var wg sync.WaitGroup
	v := []int{1,2,3}
	wg.Add(2)

	go func(ch <-chan []int, wg *sync.WaitGroup) {
		defer wg.Done()
		val, isChannelOpen := <-ch
		fmt.Println(isChannelOpen)
		fmt.Println(val)
	}(mych, &wg)

	go func(ch chan<- []int, wg *sync.WaitGroup) {
		defer wg.Done()
		ch <- v
		close(ch)
	}(mych, &wg)

	wg.Wait()

}

