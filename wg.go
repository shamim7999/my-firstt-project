package main

import (
	"fmt"
	"sync/atomic"
)
////////////////////// Manual WaitGroup Implementation //////////////////
type hello struct {
	counter int64
}

func (wg *hello) Add(n int64) {
	atomic.AddInt64(&wg.counter, n)
}

func (wg *hello) Done() {
	wg.Add(-1)
	if atomic.LoadInt64(&wg.counter) < 0 {
		fmt.Print("Negative Wait Group Counter\n")
	}
}

func (wg *hello) Wait() {
	for {
		if atomic.LoadInt64(&wg.counter) == 0 {
			return
		}
	}
}
////////////////////////////////////////////////////////////////////////
func main() {
	var wg hello
	wg.Add(10)
	for i:=1; i<=10; i++ {
		go func(x int){
			defer wg.Done()
			fmt.Println("Task",x,"completed.")
		}(i)
	}
	wg.Wait()
}
