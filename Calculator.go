package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

type calculator struct {
	res atomic.Value
}

func newCalculator() calculator {
	c := calculator{}
	c.res.Store(float64(0))
	return c
}

func (c *calculator) add(n float64) {
	c.res.Store(c.Result() + n)
}

func (c *calculator) sub(n float64) {
	c.res.Store(c.Result() - n)
}

func (c *calculator) mul(n float64) {
	c.res.Store(c.Result() * n)
}

func (c *calculator) div(n float64) {
	c.res.Store(c.Result() / n)
}

func (c *calculator) Result() float64 {
	val, ok := c.res.Load().(float64)
	if !ok {
		panic("Wrong Type\n")
	}
	return val
}

func main() {
	c := newCalculator()
	var wg sync.WaitGroup
	wg.Add(5)
	go func() {
		defer wg.Done()
		c.add(10)
		//time.Sleep(time.Second * 2)
	}()
	go func() {
		defer wg.Done()
		c.sub(5)
		//time.Sleep(time.Second * 2)
	}()
	go func() {
		defer wg.Done()
		c.div(3)
		//time.Sleep(time.Second * 2)
	}()
	go func() {
		defer wg.Done()
		c.mul(4)
		//time.Sleep(time.Second * 2)
	}()
	go func() {
		defer wg.Done()
		c.add(13)
		//time.Sleep(time.Second * 2)
	}()

	wg.Wait()

	fmt.Println(c.Result())
}

