package main

import (
	"fmt"
)

type Queue []int
type Stack []int

func main() {

	var q Queue
	var s Stack
	q.push(4)
	q.push(5)
	q.push(9)
	q.push(10)

	for !q.empty() {
		x, _ := q.pop()
		fmt.Println(x)
	}
	fmt.Println()
	s.push(4)
	s.push(5)
	s.push(9)
	s.push(10)

	for !s.empty() {
		x, _ := s.pop()
		fmt.Println(x)
	}
}

//////////// STACK ////////////

func (s *Stack) push(x int){
	*s = append(*s, x)
}

func (s *Stack) pop() (int, bool) {
	sz := len(*s)
	if sz == 0 {
		return 0, false
	}
	
	x := (*s)[sz-1]
	*s = (*s)[0:sz-1]
	return x, true
}

func (s *Stack) empty() bool {
	if len(*s) == 0 {
		return true
	}
	return false
}

/////////////// QUEUE ///////////////////

func (q *Queue) push(x int) {
	*q = append(*q, x)
	//fmt.Printf("%v is appended\n", x)
}

func (q *Queue) pop() (int, bool){
	if len(*q) == 0{
		return 0, false
	}
	x := (*q)[0]
	*q = (*q)[1:]
	return x, true
}

func (q *Queue) empty() bool{
	if len(*q) == 0{
		return true
	}
	return false
}
