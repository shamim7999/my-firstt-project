package main

import (
	"fmt"
)

type person func(int)

func func1(_ int) {
	fmt.Println("Hello from func1")
}

func func2(_ int) {
	fmt.Println("Hello from func2")
}

func func3(n int) {
	var x int
	x += n
	x += 100 
	fmt.Println(x)
}

func main() {
	mp := make(map[int]person)

	mp[12] = func1
	mp[13] = func2
	mp[14] = func3

	f := func(n int) person {
		return func(x int) {
			x = n+0
			fmt.Println("The value is: ", x)
		}
	}
	mp[15] = f(15)
	for k, v := range mp {
		fmt.Println(k, "---->")
		v(k)
		fmt.Println()
	}
}

